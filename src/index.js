const express = require('express');
const app = express();
const port = 8080;

app.get('/', (req, res) => {
  res.send('Alterdata!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

module.exports = app.listen(3000);