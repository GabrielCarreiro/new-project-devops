const supertest = require('supertest');
const app = require('./index.js');

test('Apenas um teste para fazer teste, status code', async () => {
    const response = await supertest(app).get('/');
    expect(response.statusCode).toEqual(200);
})

test('Apenas um teste para fazer teste, texto response', async () => {
  const response = await supertest(app).get('/');
  expect(response.text).toEqual('Alterdata!');
})